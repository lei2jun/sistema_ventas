<?php

Route::get('/', function () {
    return view('contenido.contenido');
});

Route::get('/contenido', function () {
    return view('contenido.contenido');
});

// CATEGORIA
Route::resource( '/categoria', 'CategoriaController' );
Route::put( '/categoria/baja/id', 'CategoriaController@darBajaEstado' );
Route::put( '/categoria/alta/id', 'CategoriaController@darAltaEstado' );
Route::get( '/categorias', 'CategoriaController@listaCategorias' );

// ARTICULO
Route::resource( '/articulo', 'ArticuloController' );
Route::put( '/articulo/baja/id', 'ArticuloController@darBajaEstado' );
Route::put( '/articulo/alta/id', 'ArticuloController@darAltaEstado' );