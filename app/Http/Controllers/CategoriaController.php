<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Categoria;
use App\Http\Requests\CrearCategoriaRequest;
use App\Http\Requests\ActualizarCategoriaRequest;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller {
    
    public function index(Request $request){
        /*if(!$request->ajax()) {
            return redirect( '/' );
        }*/

        $criterio = $request->criterio;
        $buscar = $request->buscar;

        if($buscar == '') {
            $categorias = Categoria::orderBy('id', 'desc')
                ->paginate(3);                   // eloquent
        }
        else{
            $categorias = Categoria::where($criterio, 'like', '%' . $buscar . '%')
                ->orderBy('id', 'desc')
                ->paginate(3);
        }

        //$categorias = DB::table('categorias')->paginate(5);     // query builder
        

        return [
            'paginacion' => [
                'total' => $categorias->total(),
                'pagina_actual' => $categorias->currentPage(),
                'por_pagina' => $categorias->perPage(),
                'ultima_pagina' => $categorias->lastPage(),
                'desde' => $categorias->firstItem(),
                'hasta' => $categorias->lastItem()
            ],
            'categorias' => $categorias
        ];
    }

    public function listaCategorias(Request $request) {
        $categorias = Categoria::where('estado', '=', '1')
            ->select('id', 'nombre')->orderBy('nombre', 'asc')->get();
        return [
            'categorias' => $categorias
        ];
    }

    public function create() {
        //
    }

    public function store(CrearCategoriaRequest $request) {
        $categoria = new Categoria();
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->estado = '1';
        $categoria->save();
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        //
    }

    public function update(ActualizarCategoriaRequest $request) {
        $categoria = Categoria::findOrFail( $request->id );
        $categoria->descripcion = $request->descripcion;
        $categoria->save();
    }

    public function destroy($id) {
        //
    }

    public function darBajaEstado(Request $request) {
        $categoria = Categoria::findOrFail( $request->id );
        $categoria->estado = '0';
        $categoria->save();
    }

    public function darAltaEstado(Request $request) {
        $categoria = Categoria::findOrFail( $request->id );
        $categoria->estado = '1';
        $categoria->save();
    }
}
